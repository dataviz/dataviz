$(".ratingSlider").noUiSlider({
  start: [3],
  step: 0.5,
  range: {
    'min': 0,
    'max': 5
  },
  connect: 'lower'
});

$(".budgetSlider").noUiSlider({
  start: [1],
  step: 1,
  range: {
    'min': 0,
    'max': 4
  },
  connect: 'lower'
});

$(".reviewSlider").noUiSlider({
  start: [50],
  step: 10,
  range: {
    'min': 0,
    'max': 500
  },
  connect: 'lower'
});

$(".proximitySlider").noUiSlider({
  start: [3],
  step: 0.5,
  range: {
    'min': 0,
    'max': 10
  },
  connect: 'lower'
});


function hideSlider(id) {
  $(id).addClass("hidden");
}
var clearFunc;
/**
 * Do Not Touch this method
 * Function to attach mouse enter and leave event for preference menu and slider
 */
function attachEvents(preferneceMenu, preferenceSlider) {
  $(preferneceMenu).mouseenter(function () {
    clearTimeout(clearFunc);$('.'+$(this).data("target")).removeClass("hidden");
  });$(preferneceMenu).mouseleave(function () {
    clearFunc = setTimeout(hideSlider,100,'.'+$(this).data("target"));
  });$(preferenceSlider).mouseenter(function () {
    clearTimeout(clearFunc);$(this).removeClass("hidden");
  });$(preferenceSlider).mouseleave(function () {
    clearFunc = setTimeout(hideSlider,100,this);
  });
}
attachEvents("#preference-rating-menu", "#search-places-form .preference-rating-slider");attachEvents("#preference-budget-menu", "#search-places-form .preference-budget-slider");
attachEvents("#preference-review-menu", "#search-places-form .preference-review-slider");attachEvents("#preference-proximity-menu", "#search-places-form .preference-proximity-slider");

$("#search-places-btn").click(function() {
  $("#search-places-form").find("input[name=rat]").val(parseInt($(".preference-rating-slider .ratingSlider").val()));
  $("#search-places-form").find("input[name=bud]").val(parseInt($(".preference-budget-slider .budgetSlider").val()));
  $("#search-places-form").find("input[name=rev]").val(parseInt($(".preference-review-slider .ratingSlider").val()));
  $("#search-places-form").find("input[name=pro]").val(parseInt($(".preference-proximity-slider .ratingSlider").val()));
  //alert(decodeURIComponent( $("#search-places-form").serialize() ));
  //$("#search-places-form").submit();
  //Directly submit or use ajaz here!!!!
})
