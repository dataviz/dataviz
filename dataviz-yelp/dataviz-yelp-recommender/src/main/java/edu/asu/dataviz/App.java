package edu.asu.dataviz;

import com.google.gson.Gson;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import edu.asu.dataviz.utils.DBUtils;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws  Exception
    {

        List<Document> documentList = BusinessLocator.getBusinessByLocation(-111.939945,33.422018,0.01);

        Gson gson = new Gson();
        System.out.printf(gson.toJson(documentList));
    }
}
