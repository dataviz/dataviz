package edu.asu.dataviz.utils;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * Created by arun on 3/17/15.
 */
public class DBUtils {

    /**
     * Mongo DB Instance.
     */
    private static MongoDatabase mongoDb = null;

    public static MongoDatabase getMongoDb(){
        if(mongoDb==null){
            MongoClient mongoClient = new MongoClient( "127.0.0.1" , 27017 );
            mongoDb = mongoClient.getDatabase("yelpdata");
        }
        return mongoDb;
    }


    public static MongoCollection<Document> getCollection(String collectionName){
        return getMongoDb().getCollection(collectionName);
    }

}
