package edu.asu.dataviz;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import edu.asu.dataviz.utils.DBUtils;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arun on 3/17/15.
 */
public class BusinessLocator {


    /**
     *
     * @param lat
     * @param lon
     * @param radius
     * @return
     *
     *
     *  Mongo Query:
     *      db.business_coord.find( { loc :
                { $geoWithin :
                    { $centerSphere :
                        [ [ -111.939945 , 33.422018 ] , 0.2 / 3959 ]
                } } } )
     */
    public static List<Document> getBusinessByLocation(double lat,double lon, double radius){

        ArrayList<Object> coordinates = new ArrayList<Object>();
        coordinates.add(lat);
        coordinates.add(lon);

        ArrayList<Object> centerSphere = new ArrayList<Object>();
        centerSphere.add(coordinates);
        centerSphere.add(radius/3959.00);

        Document query = new Document().append("loc", new Document("$geoWithin", new Document("$centerSphere", centerSphere)));
        query.append("categories","Restaurants");
        FindIterable<Document> iterator = DBUtils.getCollection("business_coord").find(query).limit(15);

        List<Document> docList = new ArrayList<Document>();
        for(Document document:iterator){
            String address = (String) document.get("full_address");
            address = address.replaceAll("\\n"," ");
            document.put("full_address", address);
            docList.add(document);


        }


        return docList;
    }



}
