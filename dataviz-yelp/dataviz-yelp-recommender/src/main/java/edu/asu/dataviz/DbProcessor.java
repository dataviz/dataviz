package edu.asu.dataviz;

import com.mongodb.client.MongoCollection;
import edu.asu.dataviz.utils.DBUtils;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arun on 3/17/15.
 */
public class DbProcessor {

    public static void generateLocationInformation(){
        MongoCollection<Document> collection = DBUtils.getCollection("business_location");
        MongoCollection<Document> collTwo = DBUtils.getCollection("business_coord");

        int i = 0;
        for(Document document:collection.find()){
            List<Double> coOrdinates = new ArrayList<Double>();
            coOrdinates.add(document.getDouble("longitude"));
            coOrdinates.add(document.getDouble("latitude"));
            Document newDocument= document.append("loc",new Document("type","Point").append("coordinates",coOrdinates));
            collTwo.insertOne(newDocument);

        }


        System.out.println("Complete.");
    }
}
