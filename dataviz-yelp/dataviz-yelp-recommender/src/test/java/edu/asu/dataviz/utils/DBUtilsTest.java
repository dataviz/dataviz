package edu.asu.dataviz.utils;


import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.junit.Assert;
import org.junit.Test;

public class DBUtilsTest {

    @Test
    public void testGetMongoDb() throws Exception {
        MongoDatabase db = DBUtils.getMongoDb();
        Assert.assertNotNull(db);
    }

  @Test
    public void testGetCollection() throws Exception{
        MongoCollection collection = DBUtils.getCollection("business");
        Assert.assertNotNull(collection);
    }
}