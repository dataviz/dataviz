package edu.asu.dataviz;

import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import edu.asu.dataviz.utils.DBUtils;
import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.json.simple.JSONObject;

import javax.print.Doc;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.util.*;
import java.io.File;

/**
 * Created by arun on 4/7/15.
 */
@Path("search")
public class YelpSearch {


  public static List<Document> getBusinessByLocation(double lat,double lon, double radius){

    ArrayList<Object> coordinates = new ArrayList<Object>();
    coordinates.add(lat);
    coordinates.add(lon);

    ArrayList<Object> centerSphere = new ArrayList<Object>();
    centerSphere.add(coordinates);
    centerSphere.add(radius/3959.00);

    Document query = new Document().append("loc", new Document("$geoWithin", new Document("$centerSphere", centerSphere)));
    query.append("categories","Food");
    query.append("open",true);
    FindIterable<Document> iterator = DBUtils.getCollection("business_coord").find(query);

    List<Document> docList = new ArrayList<Document>();
    for(Document document:iterator){
      String address = (String) document.get("full_address");
      address = address.replaceAll("\\n"," ");
      document.put("full_address",address);
      docList.add(document);


    }


    return docList;
  }


  public  static List<Document> getNearbyLocations(double lat,double lon, double radius){
    MongoDatabase mongoDatabase = DBUtils.getMongoDb();

    ArrayList<Object> coordinates = new ArrayList<Object>();
    coordinates.add(lat);
    coordinates.add(lon);
    Document query = new Document().append("geoNear", "business_coord").append("near",coordinates).append("spherical",true).append("limit",1000);
    Document document =  mongoDatabase.executeCommand(query);
    List<Document> documentList = (ArrayList<Document>) document.get("results");
    System.out.println(documentList.size());
    return documentList;
  }




  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String get(@Context UriInfo uriInfo) {
    MultivaluedMap<String,String> queryParams = uriInfo.getQueryParameters();
    MultivaluedMap<String,String> pathParams = uriInfo.getPathParameters();


//    Gson gson = new Gson();
//
//    return gson.toJson(queryParams);,

    List<Document> documentList = getBusinessByLocation(-111.9395536, 33.4236052, 0.3);


    for(Document document : documentList){


      ArrayList<Document> documentArrayList = new ArrayList<Document>();
      documentArrayList.add(new Document("score",100).append("label","Rating").append("color","#EFE").append("weight",1).append("order",1));
      documentArrayList.add(new Document("score",45).append("label","Value").append("color","#EFE").append("weight", 1).append("order",2));
      documentArrayList.add(new Document("score",35).append("label","Cost").append("color","#EFE").append("weight", 1).append("order",3));
      documentArrayList.add(new Document("score",82).append("label","Health").append("color","#EFE").append("weight",1).append("order",4));


      document.append("pie",documentArrayList);
    }

    Gson gson = new Gson();
    return  gson.toJson(documentList);
  }

  private static double getMaxValue(double[] arr){
    double maxValue = arr[0];

    for (int i = 1; i < arr.length; i++) {
      maxValue = Math.max(maxValue,arr[i]);
    }

    return maxValue;
  }

  private static double[] normalize(double[] arr,double max){

    for (int i = 0; i < arr.length; i++) {
      arr[i] = arr[i]/max;
    }

    return arr;
  }



  public static void main(String[] args) throws IOException {

    List<Document> businessList = getBusinessByLocation(-111.9395536, 33.4236052, 0.3);

    Map<String,Document> businessMap = new HashMap<String,Document>();
    for(Document doc:businessList){
      businessMap.put((String) doc.get("business_id"), doc);
    }

    //Calculate the distance for each business.
    List<Document> documentList = getNearbyLocations(-111.9395536, 33.4236052, 0.3);
    Set<String> businessIds = businessMap.keySet();
    for(Document document:documentList){
      double dis = document.getDouble("dis");
      Document obj = (Document) document.get("obj");
      String businessId = obj.getString("business_id");

      if(businessIds.contains(businessId)){
        businessMap.get(businessId).append("distance",dis);
      }

    };


    //Get Reviews.
    for(String businessId:businessMap.keySet()){
      Document query = new Document();
      query.append("business_id", businessId);
      FindIterable<Document> iterator = DBUtils.getMongoDb().getCollection("review_sentiment").find(query);
      int positive=0;
      int negative =0;
      for(Document document:iterator){
        double sentiment = document.getDouble("sentiment");
        if(sentiment<0.35){
          negative++;
        }else if(sentiment> 0.65){
          positive++;
        }
      }

      businessMap.get(businessId).append("posReviews",positive);
      businessMap.get(businessId).append("negReviews", negative);

    }
    System.out.println("Completed reviews.");

    //Get checkins.
    for(String businessId:businessMap.keySet()){
      Document query = new Document();
      query.append("business_id", businessId);
      FindIterable<Document> iterator = DBUtils.getMongoDb().getCollection("checkin").find(query);
      int checkins =0;
      for(Document document:iterator){
        Document checkInInfo = (Document) document.get("checkin_info");
        for(String key: checkInInfo.keySet()){
          checkins+=checkInInfo.getInteger(key);
        }
        businessMap.get(businessId).append("checkin", document.get("checkin_info"));
      }
      businessMap.get(businessId).append("total_checkins",checkins);

    }
    System.out.println("Completed Checkins.");



    //Get Tips.
    for(String businessId:businessMap.keySet()){
      Document query = new Document();
      query.append("business_id", businessId);
      FindIterable<Document> iterator = DBUtils.getMongoDb().getCollection("tip").find(query);
      List<String> tipDocs = new ArrayList<String>();
      int cntr=0;
      for(Document document:iterator){
        tipDocs.add(document.getString("text"));
        if(cntr>10){
          break;
        }
        cntr++;
      }
      businessMap.get(businessId).append("tips",tipDocs);
    }
    System.out.println("Completed Tips.");


    YelpAPI yelpAPI = new YelpAPI();

    for(String businessId:businessMap.keySet()) {
      Document document = businessMap.get(businessId);
      String id = (String) document.get("business_id");
      String name = (String) document.get("name");
      String place = (String) document.get("city");
      String business_id = name.toLowerCase().replaceAll(" ", "-").replaceAll("'", "") + "-" + place.toLowerCase().replaceAll(" ", "-");

      String json = yelpAPI.searchByBusinessId(business_id);
      org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
      String image_URL = null;
      try {
        JSONObject object = (JSONObject) parser.parse(json);
        image_URL = (String) object.get("image_url");
      } catch (Exception ex) {
        // ex.printStackTrace();
        //Do Nothing.

        if(image_URL==null){
          image_URL="https://dl.dropboxusercontent.com/u/1144931/no-thumb.png";
        }
      }
      document.append("image_url", image_URL);

    }

    int totalDocs = businessMap.size();
    double[] ratings = new double[totalDocs];
    double[] reviews = new double[totalDocs];
    double[] checkins = new double[totalDocs];
    double[] proximity = new double[totalDocs];
    double[] price = new double[totalDocs];


    int counter=0;
    //Calculate the yelp score.
    for(String businessId:businessMap.keySet()){
      Document document = businessMap.get(businessId);
      ratings[counter] = document.getDouble("stars");
      reviews[counter] = (double)document.getInteger("review_count");
      proximity[counter] = document.getDouble("distance");
      System.out.println(counter);
      checkins[counter] = document.getInteger("total_checkins");
      Document attributes = (Document) document.get("attributes");
      if(attributes.containsKey("Price Range"))
        price[counter] = attributes.getInteger("Price Range");

      counter++;
    }

    //Calculate Max values.
    double maxRating = getMaxValue(ratings);
    double maxReviews = getMaxValue(reviews);
    double maxCheckins = getMaxValue(checkins);
    double maxProximity = getMaxValue(proximity);
    double maxPrice = getMaxValue(price);


    //Normalize.
    ratings = normalize(ratings,maxRating);
    checkins = normalize(checkins,maxCheckins);
    reviews = normalize(reviews,maxReviews);
    proximity = normalize(proximity,maxProximity);
    price = normalize(price,maxPrice);


    int[] yelpScore = new int[totalDocs];
    //Calculate YELP Score.
    for (int i = 0; i < totalDocs; i++) {
      yelpScore[i]= (int)Math.round ((0.4*ratings[i] + 0.15*checkins[i] + 0.15*reviews[i] + 0.15*(1.0-proximity[i]) + 0.15*(1-price[i]))*100);
    }


    //Get the top 10 scores.
    counter=0;
    Set<String> treeList = new TreeSet<String>();
    //Calculate the yelp score.
    for(String businessId:businessMap.keySet()){
      treeList.add(yelpScore[counter]+"##"+businessId);
      businessMap.get(businessId).append("yscore", yelpScore[counter]);
      businessMap.get(businessId).append("yscore_rating",ratings[counter]);
      businessMap.get(businessId).append("yscore_reviews",reviews[counter]);
      businessMap.get(businessId).append("yscore_checkin",checkins[counter]);
      businessMap.get(businessId).append("yscore_proximity",(1.0-proximity[counter]));
      businessMap.get(businessId).append("yscore_price",(1.0-price[counter]));

      counter++;
    }


    Iterator<String> iterator = treeList.iterator();
    List<Document> tempList = new ArrayList<Document>();
    while(iterator.hasNext()){
      String key = iterator.next();
      String business_id = key.split("##")[1];
      tempList.add(businessMap.get(business_id));
    }

    List<Document> finalList = new ArrayList<Document>();
    for (int i = 0; i < 10; i++) {
      finalList.add(tempList.get(totalDocs-i-1));
    }
    
    //Add metadata.
    for (int i = 0; i < 10; i++) {
      Document document = finalList.get(i);
      List<Map<String,String>> attributes = new ArrayList<Map<String, String>>();
      //Rating
      Map<String,String> attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#F0B201");
      attributeMap.put("id","FIS");
      attributeMap.put("label","Rating");
      attributeMap.put("order","1");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_rating"))+"");
      attributeMap.put("weight", "0.4");
      attributes.add(attributeMap);

      //Checkins
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#9b59b6");
      attributeMap.put("id","CI");
      attributeMap.put("label","Checkins");
      attributeMap.put("order","2");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_checkin"))+"");
      attributeMap.put("weight", "0.15");
      attributes.add(attributeMap);

      //Reviews
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#33CC33");
      attributeMap.put("id","NP");
      attributeMap.put("label","Review");
      attributeMap.put("order","3");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_reviews"))+"");
      attributeMap.put("weight", "0.15");
      attributes.add(attributeMap);

      //Reviews
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#FF0066");
      attributeMap.put("id","AO");
      attributeMap.put("label","Price");
      attributeMap.put("order","4");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_price"))+"");
      attributeMap.put("weight", "0.15");
      attributes.add(attributeMap);

      //Reviews
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#555");
      attributeMap.put("id","CS");
      attributeMap.put("label","Proximity");
      attributeMap.put("order","5");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_proximity"))+"");
      attributeMap.put("weight","0.15");
      attributes.add(attributeMap);

      document.append("marker",attributes);
    }

    //Generate JSON File.
      Gson gson = new Gson();
      String string = gson.toJson(finalList);
    System.out.println(string);
    FileUtils fileUtils = new FileUtils();
    fileUtils.writeStringToFile(new File("/home/arun/Desktop/search2.json"),string);


  }




}
