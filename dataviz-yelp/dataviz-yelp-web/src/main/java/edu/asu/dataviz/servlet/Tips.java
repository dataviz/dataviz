package edu.asu.dataviz.servlet;


import com.google.gson.Gson;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import edu.asu.dataviz.YSearch;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by arun on 5/5/15.
 */
public class Tips extends HttpServlet {


  public void init() throws ServletException {
    // Do required initialization
  }

  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    //Fetch Parameters.
    String ratings = request.getParameter("rat");
    String budget = request.getParameter("bud");
    String reviews = request.getParameter("rev");
    String proximity = request.getParameter("pro");
    String lat = request.getParameter("lat");
    String lon = request.getParameter("lng");
    String category = request.getParameter("category");

    Map<String,String> returnValues = new HashMap<>();


    YSearch ySearch = new YSearch();

//    String json = ySearch.tips(business_id);

    response.setContentType("application/json");
    PrintWriter out = response.getWriter();
//    out.print(json);
    out.flush();


  }
}
