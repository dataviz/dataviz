package edu.asu.dataviz.servlet;

import com.google.gson.Gson;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by arun on 5/5/15.
 */
public class LocationServlet extends HttpServlet {


  public void init() throws ServletException {
    // Do required initialization
  }

  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    //Fetch Parameters.
    String address01 = request.getParameter("address01");
    String address02 = request.getParameter("address02");
    String city = request.getParameter("map-cities");

    GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyDtpMOxZek12ARGv4ov1wl5PDMffbvZzCg");

    GeocodingResult[] results=null;
    Map<String,String> returnValues = new HashMap<>();

    try{
      results =  GeocodingApi.geocode(context, address01+", "+address02+", "+city).await();

    }catch (Exception ex){
      returnValues.put("status","404");
    }

    if(results==null){
      returnValues.put("status","404");
    }else{
      returnValues.put("status","200");
      returnValues.put("address",results[0].formattedAddress);
      returnValues.put("lat",results[0].geometry.location.lat+"");
      returnValues.put("lng",results[0].geometry.location.lng+"");
    }
    Gson gson = new Gson();
    String json = gson.toJson(returnValues);

    response.setContentType("application/json");
    PrintWriter out = response.getWriter();
    out.print(json);
    out.flush();


  }
}
