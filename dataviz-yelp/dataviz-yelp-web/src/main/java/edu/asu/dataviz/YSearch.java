package edu.asu.dataviz;

import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import edu.asu.dataviz.utils.DBUtils;
import org.bson.Document;
import org.json.simple.JSONObject;

import java.util.*;

/**
 * Created by arun on 5/5/15.
 */
public class YSearch {


  public static final double R = 3959.00; // In kilometers

  public static void main(String[] args) {
    YSearch ySearch = new YSearch();
//    ySearch.search("2.5","2","100","2.0","33.4236052","-111.9395536");
  }


  public static double haversine(double lat1, double lon1, double lat2, double lon2) {
    double dLat = Math.toRadians(lat2 - lat1);
    double dLon = Math.toRadians(lon2 - lon1);
    lat1 = Math.toRadians(lat1);
    lat2 = Math.toRadians(lat2);

    double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    double c = 2 * Math.asin(Math.sqrt(a));
    return R * c;
  }

  private  double getMaxValue(double[] arr){
    double maxValue = 0;

    for (int i = 0; i < arr.length; i++) {
      maxValue = Math.max(maxValue,arr[i]);
    }

    return maxValue;
  }

  private  double[] normalize(double[] arr,double max){

    for (int i = 0; i < arr.length; i++) {
      arr[i] = arr[i]/max;
    }

    return arr;
  }


  private List<Document> getBusinessByLocation(double lat,double lon, double radius, double rating, int budget, int review,String category){

    ArrayList<Object> coordinates = new ArrayList<Object>();
    coordinates.add(lon);
    coordinates.add(lat);

    ArrayList<Object> centerSphere = new ArrayList<Object>();
    centerSphere.add(coordinates);
    centerSphere.add(radius / R);

    Document query = new Document().append("loc", new Document("$geoWithin", new Document("$centerSphere", centerSphere)));
    query.append("categories",category);
    query.append("stars",new Document("$gte",rating));
    System.out.println(budget);
    query.append("attributes.Price Range", new Document("$gte", budget));
    query.append("review_count",new Document("$gte",review));

    query.append("open",true);
    FindIterable<Document> iterator = DBUtils.getCollection("business_coord").find(query);

    List<Document> docList = new ArrayList<Document>();
    for(Document document:iterator){
      String address = (String) document.get("full_address");
      address = address.replaceAll("\\n"," ");
      document.put("full_address",address);
      docList.add(document);
    }

    return docList;
  }

  public String search(String ratings,String budget,String reviews,String prox,String lat,String lon,String category){

    double startTime = System.currentTimeMillis();
    double latitude = Double.parseDouble(lat);
    double longitude = Double.parseDouble(lon);
    double proximity = Double.parseDouble(prox);


    double ratingScore = Double.parseDouble(ratings);
    int budgetScore = Integer.parseInt(budget);
    int reviewScore = Integer.parseInt(reviews);

    //Get all the documents based on lat, lon and other options.
    List<Document> businessList = getBusinessByLocation(latitude, longitude, proximity,ratingScore,budgetScore,reviewScore,category);

    System.out.println(businessList.size());
    //Populate all businesses into map.
    Map<String,Document> businessMap = new HashMap<String,Document>();
    for(Document doc:businessList){
      businessMap.put((String) doc.get("business_id"), doc);
    }


    //Calculate the distance from each business
    for(Document document:businessList){
      double distance = haversine(document.getDouble("latitude"), document.getDouble("longitude"),latitude,longitude);
      String businessId = document.getString("business_id");
      businessMap.get(businessId).append("distance", distance);
    }

    //Get Reviews.
    for(String businessId:businessMap.keySet()){
      Document document = businessMap.get(businessId);
      document.append("posReviews",document.getInteger("pos"));
      document.append("negReviews",document.getInteger("neg"));

    }

    for(String businessId:businessMap.keySet()){
      Document document = businessMap.get(businessId);
      if(document.get("total_checkins")==null){
        document.append("total_checkins",0);
      }
    }


    //Get Tips.
//    for(String businessId:businessMap.keySet()){
//      Document query = new Document();
//      query.append("business_id", businessId);
//      FindIterable<Document> iterator = DBUtils.getMongoDb().getCollection("tip").find(query);
//      List<String> tipDocs = new ArrayList<String>();
//      int cntr=0;
//      for(Document document:iterator){
//        tipDocs.add(document.getString("text"));
//        if(cntr>10){
//          break;
//        }
//        cntr++;
//      }
//      businessMap.get(businessId).append("tips",tipDocs);
//    }
//    System.out.println("Completed Tips.");


    //Finalize the list of businesses.
    int totalDocs = businessMap.size();
    double[] ratingsArray = new double[totalDocs];
    double[] reviewsArray = new double[totalDocs];
    double[] checkinsArray = new double[totalDocs];
    double[] proximityArray = new double[totalDocs];
    double[] priceArray = new double[totalDocs];


    int counter=0;
    //Calculate the yelp score.
    for(String businessId:businessMap.keySet()){
      Document document = businessMap.get(businessId);
      ratingsArray[counter] = document.getDouble("stars");
      reviewsArray[counter] = (double)document.getInteger("review_count");
      proximityArray[counter] = document.getDouble("distance");
      checkinsArray[counter] = document.getInteger("total_checkins");
      
      Document attributes = (Document) document.get("attributes");
      if(attributes.containsKey("Price Range"))
        priceArray[counter] = attributes.getInteger("Price Range");

      counter++;
    }

    //Calculate Max values.
    double maxRating = getMaxValue(ratingsArray);
    double maxReviews = getMaxValue(reviewsArray);
    double maxCheckins = getMaxValue(checkinsArray);
    double maxProximity = getMaxValue(proximityArray);
    double maxPrice = getMaxValue(priceArray);



    //Normalize.
    ratingsArray = normalize(ratingsArray,maxRating);
    checkinsArray = normalize(checkinsArray,maxCheckins);
    reviewsArray = normalize(reviewsArray,maxReviews);
    proximityArray = normalize(proximityArray,maxProximity);
    priceArray = normalize(priceArray,maxPrice);


    int[] yelpScore = new int[totalDocs];
    //Calculate YELP Score.
    for (int i = 0; i < totalDocs; i++) {
      yelpScore[i]= (int)Math.round ((0.4*ratingsArray[i] + 0.15*checkinsArray[i] + 0.15*reviewsArray[i] + 0.15*(1.0-proximityArray[i]) + 0.15*(1-priceArray[i]))*100);
    }


    //Get the top 10 scores.
    counter=0;
    Set<String> treeList = new TreeSet<String>();
    //Calculate the yelp score.
    for(String businessId:businessMap.keySet()){
      treeList.add(yelpScore[counter]+"##"+businessId);
      businessMap.get(businessId).append("yscore", yelpScore[counter]);
      businessMap.get(businessId).append("yscore_rating",ratingsArray[counter]);
      businessMap.get(businessId).append("yscore_reviews",reviewsArray[counter]);
      businessMap.get(businessId).append("yscore_checkin",checkinsArray[counter]);
      businessMap.get(businessId).append("yscore_proximity",(1.0-proximityArray[counter]));
      businessMap.get(businessId).append("yscore_price",(1.0-priceArray[counter]));

      counter++;
    }


    Iterator<String> iterator = treeList.iterator();
    List<Document> tempList = new ArrayList<Document>();
    while(iterator.hasNext()){
      String key = iterator.next();
      String business_id = key.split("##")[1];
      tempList.add(businessMap.get(business_id));
    }



    List<Document> finalList = new ArrayList<Document>();
    for (int i = 0; i < Math.min(totalDocs,10); i++) {
      finalList.add(tempList.get(totalDocs-i-1));
    }

    //Get Tips.
    for(Document document:finalList){
      Document query = new Document();
      query.append("business_id", document.getString("business_id"));
      FindIterable<Document> tipoiterator = DBUtils.getMongoDb().getCollection("tip").find(query);
      List<String> tipDocs = new ArrayList<String>();
      int cntr=0;
      for(Document tip:tipoiterator){
        tipDocs.add(tip.getString("text"));
        if(cntr>10){
          break;
        }
        cntr++;
      }
      document.append("tips",tipDocs);
    }

    //Add map metadata.
    for (int i = 0; i < finalList.size(); i++) {
      Document document = finalList.get(i);
      List<Map<String,String>> attributes = new ArrayList<Map<String, String>>();
      //Rating
      Map<String,String> attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#F0B201");
      attributeMap.put("id","FIS");
      attributeMap.put("label","Rating");
      attributeMap.put("order","1");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_rating"))+"");
      attributeMap.put("weight", "0.4");
      attributes.add(attributeMap);

      //Checkins
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#9b59b6");
      attributeMap.put("id","CI");
      attributeMap.put("label","Checkins");
      attributeMap.put("order","2");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_checkin"))+"");
      attributeMap.put("weight", "0.15");
      attributes.add(attributeMap);

      //Reviews
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#33CC33");
      attributeMap.put("id","NP");
      attributeMap.put("label","Review");
      attributeMap.put("order","3");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_reviews"))+"");
      attributeMap.put("weight", "0.15");
      attributes.add(attributeMap);

      //Reviews
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#FF0066");
      attributeMap.put("id","AO");
      attributeMap.put("label","Price");
      attributeMap.put("order","4");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_price"))+"");
      attributeMap.put("weight", "0.15");
      attributes.add(attributeMap);

      //Reviews
      attributeMap = new HashMap<String, String>();
      attributeMap.put("color","#555");
      attributeMap.put("id","CS");
      attributeMap.put("label","Proximity");
      attributeMap.put("order","5");
      attributeMap.put("score",(int)(100*document.getDouble("yscore_proximity"))+"");
      attributeMap.put("weight","0.15");
      attributes.add(attributeMap);

      document.append("marker",attributes);
    }


    for (Document document:finalList){
      System.out.println(document.get("name"));
      System.out.println(document.get("full_address"));
      System.out.println("distance--"+haversine(document.getDouble("latitude"), document.getDouble("longitude"), latitude, longitude));
      System.out.println("YScore: "+document.getInteger("yscore"));
      System.out.println("Review Count: "+document.getInteger("review_count"));

      Document attributes = (Document) document.get("attributes");
      System.out.println("Price Range: "+ attributes.getInteger("Price Range"));



      System.out.println("\n");
    }

    double endTime = System.currentTimeMillis();

    System.out.println("Execution time: "+((endTime-startTime)/1000.0)+" seconds.");
    Gson gson = new Gson();
    String resString = gson.toJson(finalList);

    return resString;
  }


}
