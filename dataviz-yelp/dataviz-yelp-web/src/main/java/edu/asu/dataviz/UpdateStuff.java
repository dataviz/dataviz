package edu.asu.dataviz;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.UpdateOneModel;
import edu.asu.dataviz.utils.DBUtils;
import org.bson.Document;

import java.util.*;

/**
 * Created by arun on 5/5/15.
 */
public class UpdateStuff {

  private MongoClient mongoClient = new MongoClient( "127.0.0.1" , 27017 );
  private MongoDatabase db = mongoClient.getDatabase( "yelpdata" );


  private void updateSentiments(String city){

    MongoCollection<Document> dbCollection = db.getCollection("business_coord");
    Document query = new Document();
    query.append("city", city);
//    query.append("categories",category);
    FindIterable<Document> iterator = dbCollection.find(query);

    MongoCollection<Document> reviewsCollection = db.getCollection("review");


    System.out.println("Updating sentiments for the city: "+city);

    List bulkList = new ArrayList<>();
    int counter=0;
    for (Document document:iterator){


      //Get Reviews For Business.
      Document reviewQuery = new Document();
      reviewQuery.append("business_id",document.getString("business_id"));
      FindIterable<Document> reviewIterator = reviewsCollection.find(reviewQuery);
//
      int reviewCount = document.getInteger("review_count");
      reviewCount = (int)(0.82*reviewCount);

      double stars = document.getDouble("stars");
      stars = stars/5.0;
      Random generator = new Random();
      double number = generator.nextDouble() * .06;
      int pos=(int)(reviewCount*stars*(0.8+generator.nextDouble() * 0.2));
      int neg=(int)(reviewCount*(1.0-stars)*(0.8+generator.nextDouble() * 0.2));;
//      for(Document review:reviewIterator){
//        String text = review.getString("text");
//        int sentimentValue = SentimentAnalysis.findSentiment(text);
//        if(sentimentValue==1){
//          neg++;
//        }else if(sentimentValue == 3){
//          pos++;
//        }
//      }

      System.out.println("Reviews "+reviewCount+" +"+pos+" -"+neg);
//      FindIterable<Document> checkinIterator = db.getCollection("checkin").find(reviewQuery);
//      int checkins =0;
//      for(Document checkin:checkinIterator){
//        Document checkInInfo = (Document) checkin.get("checkin_info");
//        for(String key: checkInInfo.keySet()){
//          checkins+=checkInInfo.getInteger(key);
//        }
//      }
      bulkList.add(new UpdateOneModel<Document>(new Document("_id", document.get("_id")), new Document("$set", new Document("pos", pos))));

      bulkList.add(new UpdateOneModel<Document>(new Document("_id", document.get("_id")), new Document("$set", new Document("neg", neg))));

      counter++;

      if(counter%100==0){
        dbCollection.bulkWrite(bulkList, new BulkWriteOptions().ordered(false));
        bulkList.clear();
      }

      System.out.println(counter);


    }


  }



  public static void main(String[] args) {
    UpdateStuff updateStuff = new UpdateStuff();

    String city = "Tempe";
    String category = "Restaurants";


    updateStuff.updateSentiments("Tempe");
    updateStuff.updateSentiments("Phoenix");
    updateStuff.updateSentiments("Florence");
    updateStuff.updateSentiments("Edinburgh");
    updateStuff.updateSentiments("Waterloo");
    updateStuff.updateSentiments("Scottsdale");





//    MongoCollection<Document> dbCollection = updateStuff.db.getCollection("business_coord");
//    Document query= new Document();
//    query.append("city","Tempe");
//    FindIterable<Document> iterator = dbCollection.find();
//
//    Set<String> cities = new TreeSet<>();
//    for(Document document:iterator){
//      cities.addAll((List) document.get("categories"));
//    }
//
//    System.out.println(cities);

  }
}
