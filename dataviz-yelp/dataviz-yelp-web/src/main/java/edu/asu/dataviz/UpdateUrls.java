package edu.asu.dataviz;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.UpdateOneModel;
import org.bson.Document;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arun on 4/26/15.
 */
public class UpdateUrls {

  public static void main(String[] args) {
    UpdateUrls updateUrls = new UpdateUrls();
    MongoClient mongoClient = new MongoClient( "127.0.0.1" , 27017 );
    MongoDatabase db = mongoClient.getDatabase( "yelpdata" );

    MongoCollection<Document> dbCollection = db.getCollection("business_coord");
    Document query = new Document();
    query.append("city","Edinburgh");
    FindIterable<Document> iterator = dbCollection.find(query);


    YelpAPI yelpAPI = new YelpAPI();

    int counter = 0;
    List bulkList = new ArrayList<>();
    for(Document document:iterator){

      String id = (String) document.get("business_id");
      String name = (String) document.get("name");
      String place = (String) document.get("city");

      String business_id = name.toLowerCase().replaceAll(" ","-").replaceAll("'","")+"-"+place.toLowerCase().replaceAll(" ","-");

      String json = yelpAPI.searchByBusinessId(business_id);

      org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
      String image_URL = null;
      try{
        JSONObject object = (JSONObject) parser.parse(json);
        image_URL = (String) object.get("image_url");
      }catch (Exception ex){
        // ex.printStackTrace();
        //Do Nothing.
      }

      if(image_URL==null){
        image_URL="https://dl.dropboxusercontent.com/u/1144931/no-thumb.png";
      }


      bulkList.add(new UpdateOneModel<Document>(new Document("_id", document.get("_id")), new Document("$set", new Document("image_url", image_URL))));

      counter++;

      if(counter%100==0){
        long startTime = System.currentTimeMillis();
        dbCollection.bulkWrite(bulkList,new BulkWriteOptions().ordered(false));
        bulkList.clear();
        long endTime = System.currentTimeMillis();
        System.out.println("Execution time: "+((endTime-startTime)/1000.0)+" seconds.");

      }

//      System.out.println((counter++)+":"+document.getString("name"));
    }
  }
}
