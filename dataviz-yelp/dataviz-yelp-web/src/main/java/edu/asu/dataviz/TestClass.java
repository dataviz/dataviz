package edu.asu.dataviz;

import com.mongodb.client.FindIterable;
import edu.asu.dataviz.utils.DBUtils;
import jdk.nashorn.internal.parser.JSONParser;
import org.bson.Document;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arun on 4/26/15.
 */
public class TestClass {


  public static List<Document> getBusinessByLocation(double lat,double lon, double radius){

    ArrayList<Object> coordinates = new ArrayList<Object>();
    coordinates.add(lat);
    coordinates.add(lon);

    ArrayList<Object> centerSphere = new ArrayList<Object>();
    centerSphere.add(coordinates);
    centerSphere.add(radius / 3959.00);

    Document query = new Document().append("loc", new Document("$geoWithin", new Document("$centerSphere", centerSphere)));
    query.append("categories","Restaurants");
    query.append("open",true);
    FindIterable<Document> iterator = DBUtils.getCollection("business_coord").find(query);

    List<Document> docList = new ArrayList<Document>();

    for(Document document:iterator){
      String address = (String) document.get("full_address");
      address = address.replaceAll("\\n"," ");
      document.put("full_address",address);
      docList.add(document);


    }


    return docList;
  }


  public static void main(String[] args) {
    List<Document> documentList = getBusinessByLocation(-111.9395536, 33.4236052, 0.2);


    YelpAPI yelpAPI = new YelpAPI();


    double[] totalRatings = new double[documentList.size()];
    int[] no_of_reviews = new int[documentList.size()];



    for(Document document:documentList){
      String id = (String) document.get("business_id");
      String name = (String) document.get("name");
      String place = (String) document.get("city");

      System.out.println(id+"--"+name);

//      String business_id = name.toLowerCase().replaceAll(" ","-").replaceAll("'","")+"-"+place.toLowerCase().replaceAll(" ","-");
//
//      String json = yelpAPI.searchByBusinessId(business_id);
//
//      org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
//      String image_URL = null;
//      try{
//        JSONObject object = (JSONObject) parser.parse(json);
//        image_URL = (String) object.get("image_url");
//      }catch (Exception ex){
//       // ex.printStackTrace();
//        //Do Nothing.
//      }
//
//      System.out.println(image_URL);

    }
  }
}
