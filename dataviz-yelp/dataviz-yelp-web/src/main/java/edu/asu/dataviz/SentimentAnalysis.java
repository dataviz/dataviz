package edu.asu.dataviz;

import com.mongodb.*;

import java.io.IOException;
import java.util.Properties;
import java.io.*;

//import edu.stanford.nlp.ling.CoreAnnotations;
//import edu.stanford.nlp.rnn.RNNCoreAnnotations;
//import edu.stanford.nlp.pipeline.Annotation;
//import edu.stanford.nlp.pipeline.StanfordCoreNLP;
//import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
//import edu.stanford.nlp.trees.Tree;
//import edu.stanford.nlp.util.CoreMap;

import java.lang.Math;


public class SentimentAnalysis {

  private static Properties props = new Properties();
  static{
    props.setProperty("annotators", " tokenize, ssplit, parse, sentiment");
  }
//  private static StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
//
//	public static int findSentiment(String tweet) {
//
//
//        Annotation document = new Annotation(tweet);
//        pipeline.annotate(document);
//        int sentiment=0;
//        int neg=0;
//        int neut=0;
//        int pos=0;
//            for (CoreMap sentence : document.get(CoreAnnotations.SentencesAnnotation.class)) {
//                Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
//                sentiment = RNNCoreAnnotations.getPredictedClass(tree);
//                String partText = sentence.toString();
//                if(sentiment==1)
//                	neg=neg+1;
//                else if(sentiment==2)
//                	neut=neut+1;
//                else
//                	pos=pos+1;
//            }
//            //System.out.println("neg: "+neg+" neut: "+neut+" pos: "+pos);
//            if(neg>=pos)
//            {
//            	if(neg>=neut)
//            		return 1;
//            	else
//            	{
//            		if(neut>=pos)
//            			return 2;
//            		else
//            			return 3;
//            	}
//            }
//            else
//            {
//            	if(pos>neut)
//            		return 3;
//            	else
//            	{
//            		if(neg>neut)
//            			return 1;
//            		else
//            			return 2;
//            	}
//            }
//    }

    public double getSentiment(String sentence){
    	
    	//System.out.println(sentence);
//    	return(findSentiment(sentence));
    	return 0.0;
    }

    public static void main(String[] args) throws Exception {

        SentimentAnalysis sentimentAnalysis = new SentimentAnalysis();
        MongoClient mongoClient = new MongoClient( "129.219.208.44" , 27017 );

        DB db = mongoClient.getDB( "yelpdata" );

        //Get review collection from DB.
        DBCollection dbCollection = db.getCollection("review_sentiment");
        System.out.println("Total Count: "+dbCollection.getCount());

        //Iterate through documents.
        DBCursor cursor = dbCollection.find();
        while(cursor.hasNext()){
            DBObject object = cursor.next();

            //Get the text field.
            String reviewText = (String)object.get("text");

            //Use the getSentiment method to fetch sentiment score -1 or 1 based on the text.
            double sentiScore = sentimentAnalysis.getSentiment(reviewText);
            //System.out.println("Sentiment score is: "+sentiScore);
            object.put("sentiment",sentiScore);

            dbCollection.save(object);
        }
    }
}
