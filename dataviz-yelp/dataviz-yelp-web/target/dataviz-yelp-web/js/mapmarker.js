/*
 * To place the markers of the activities on map
 */

var CITY_LAT_LNG = new google.maps.LatLng(33.4236052,-111.9395536);//BrickYard, Tempe
//var GOOG_PLACES_SERVICE = new google.maps.places.PlacesService(GOOG_MAP);
var GOOG_MAPS_INFO_WINDOW = new google.maps.InfoWindow();

//Center and zoom to current city
var map_options = {
	center: CITY_LAT_LNG,
	zoom: 20,
	zoomControl: false,
	mapTypeControl: false,
	panControl: false,
	scrollwheel: false,
	draggable: true,
	navigationControl: true,
	mapTypeId: google.maps.MapTypeId.ROADMAP
};

var GOOG_MAP = new google.maps.Map(document.getElementById("map"), map_options);

createMarkerUsingLatLong(33.4236052, -111.9395536);

function createMarkerUsingLatLong(lat, lng) {
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat,lng),
		map: GOOG_MAP,
		icon: './img/you-are-here64.png'
	});
    google.maps.event.addListener(marker, 'mouseover', (function(marker) {
        return function() {
            GOOG_MAPS_INFO_WINDOW.setContent("You are here! Click to change location.");
            GOOG_MAPS_INFO_WINDOW.open(GOOG_MAP, marker);
        };
    })(marker));
    google.maps.event.addListener(marker, 'mouseout', (function(marker) {
        return function() {
            GOOG_MAPS_INFO_WINDOW.close();  
        };
    })(marker));

	google.maps.event.addListener(marker, 'click', function () {
		showLocationModal();
	});
}

function createMarker() {
	var marker = new google.maps.Marker({
		position: CITY_LAT_LNG,
		map: GOOG_MAP,
		icon: './img/you-are-here64.png'
	});
	google.maps.event.addListener(marker, 'click', function () {
		showLocationModal();
	});
}

$(".hero-wrapper").css("height", window.innerHeight + 'px');
//$(".hero-wrapper").css("height", $("body").css("height"));

function showLocationModal(){
        console.log("Working..");
        $('#myModal').modal('show');
}

function reloadCurrentLocation(lat,lng){
    //Center the map based on given latitude and longitude..
    CITY_LAT_LNG = new google.maps.LatLng(lat,lng);

    //Center and zoom to current city
    var map_options = {
        center: CITY_LAT_LNG,
        zoom: 18,
        zoomControl: false,
        mapTypeControl: false,
        panControl: false,
        scrollwheel: false,
        draggable: true,
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    GOOG_MAP = new google.maps.Map(document.getElementById("map"), map_options);
		createMarkerUsingLatLong(lat,lng);
}

$(document).ready(function(){
   $('#location-submit-btn').click(function(){
       $.get("http://129.219.208.44:8080/dataviz-yelp-web/location?"+$('#location-form').serialize(),function(data){
         if(data.status==="200"){
             console.log("Success");
             $('#address').html(data.address).show();

             //Latitude
             var lat = data.lat;
             $('#hidden-lat').val(lat);

             //Longitude
             var lng = data.lng;
             $('#hidden-lng').val(lng);

             //Center the map.
             reloadCurrentLocation(lat,lng);
             createMarker(lat,lng);

         }else{
             console.log("Error");
         }
       });
       $('#myModal').modal('hide');
   });
});
