var RESULTS_LIST_ITEMS_TEMPLATE_BEGIN_SRC = $("#results-list-items-template-begin").html();
var RESULTS_LIST_ITEMS_TEMPLATE_BEGIN = Handlebars.compile(RESULTS_LIST_ITEMS_TEMPLATE_BEGIN_SRC);

var RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_BEGIN_SRC = $("#results-list-items-details-template-begin").html();
var RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_BEGIN = Handlebars.compile(RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_BEGIN_SRC);

var RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_END_SRC = $("#results-list-items-details-template-end").html();
var RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_END = Handlebars.compile(RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_END_SRC);

var GOOG_MAPS_INFO_WINDOW = new google.maps.InfoWindow();
var GOOG_MAP;
var RESULT_MAP_OPTION;
var CITY_LAT_LNG;
var DEFAULT_LAT = 33.4236052;
var DEFAULT_LNG = -111.9395536;
/*
 * Get the URL parameters and then make JSON call
 */
var URL_PARAMETERS;
var HOSTNAME_FOR_SEARCH = "/dataviz-yelp-web/search";
$(function() {
	URL_PARAMETERS = window.location.search;
	console.log("URL Paramter" + URL_PARAMETERS);
	setSliderValues();

	CITY_LAT_LNG = returnLatLng(URL_PARAMETERS.split('&'));
	RESULT_MAP_OPTION = {
		center: CITY_LAT_LNG,
		zoom: 15,
		zoomControlOptions: {
	        style: google.maps.ZoomControlStyle.SMALL,
	        position: google.maps.ControlPosition.LEFT_CENTER
	    },
		zoomControl: true,
		mapTypeControl: false,
		panControl: true,
		scrollwheel: false,
		draggable: true,
		navigationControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		featureType: "poi",
		stylers: [
			{ visibility: "off" }
		]
	};

	GOOG_MAP = new google.maps.Map(document.getElementById("map"), RESULT_MAP_OPTION);
	var youAreHereMarker = new google.maps.Marker({
		position: CITY_LAT_LNG,
		map: GOOG_MAP,
		icon: './img/you-are-here64.png'
	});
	google.maps.event.addListener(youAreHereMarker, 'mouseover', (function(youAreHereMarker) {
		return function() {
			GOOG_MAPS_INFO_WINDOW.setContent("You are here!");
			GOOG_MAPS_INFO_WINDOW.open(GOOG_MAP, youAreHereMarker);
		};
	})(youAreHereMarker));
	google.maps.event.addListener(youAreHereMarker, 'mouseout', (function(youAreHereMarker) {
		return function() {
			GOOG_MAPS_INFO_WINDOW.close();
		};
	})(youAreHereMarker));

	$(".hero-wrapper").css("height", window.innerHeight + 'px');

	$.getJSON(HOSTNAME_FOR_SEARCH+URL_PARAMETERS,function(data){
	    console.log(data);
	    for (var i = 0; i < data.length; i++) {
	    	var resultsItemCode = "";
	    	if (i != 0) {
	    		resultsItemCode += "<hr class='results-divider divider'>";
	    	}
	    	resultsItemCode += RESULTS_LIST_ITEMS_TEMPLATE_BEGIN(data[i]);
	    	resultsItemCode += generateStarRatingCode(data[i].stars);
	    	resultsItemCode += '</div>';
	    	resultsItemCode += generateBusinessOpenCode(data[i].open);
	    	resultsItemCode += resultsItemCodeEnd();
	    	$("#results-list-id").append(resultsItemCode);

	    	//BUsiness description code
	    	var businessDescCode = "";
	    	businessDescCode += RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_BEGIN(data[i]);
	    	businessDescCode += generateStarRatingCode(data[i].stars);
	    	businessDescCode += "</div></div><div class='col-xs-5'><div class='text-right'>";
	    	businessDescCode += generatePriceRange(data[i].attributes["Price Range"]);
	    	businessDescCode += "</div></div>";
	    	businessDescCode += RESULTS_LIST_ITEMS_DETAILS_TEMPLATE_END(data[i]);
	    	$("#business-details-items-list-id").append(businessDescCode);
	    	console.log(data[i].marker);

	    	//create marker on map
	    	createMarker(data[i].latitude,data[i].longitude, data[i].name, data[i].business_id, data[i].marker, data[i].yscore);
	    };
	    //By default make thedescription of first item in JSON visible
	    $("#"+data[0].business_id+"-desc").removeClass("hidden");
	    //Attach hover event
	    $("#results-list-id .results-item").hover(function() {
	    	//reset all the item description
	    	$(".results-item-description").addClass("hidden");
	    	//Make the respective description visible
	    	$("#"+$(this).data("target")).removeClass("hidden");
	    })
	});

	//Attach slider events
	$("#ratings-slider-id").on({
		change: function() {
			$("#ratings-icon-id").empty();
			$("#ratings-icon-id").append(generateStarRatingCode(Number.parseFloat($("#ratings-slider-id").val())));
			window.location.search = changeUrlParam("rat",Number.parseFloat($("#ratings-slider-id").val()));
		}
	});
	$("#budget-slider-id").on({
		change: function() {
			$("#budget-icon-id").empty();
			$("#budget-icon-id").append(generatePriceRange(Number.parseInt($("#budget-slider-id").val())));
			window.location.search = changeUrlParam("bud",Number.parseInt($("#budget-slider-id").val()));
		}
	});
	$("#reviews-slider-id").on({
		change: function() {
			$("#reviews-icon-id").empty();
			$("#reviews-icon-id").append(generateReviewsIcon(Number.parseInt($("#reviews-slider-id").val())));
			window.location.search = changeUrlParam("rev",Number.parseInt($("#reviews-slider-id").val()));
		}
	});
	$("#proximity-slider-id").on({
		change: function() {
			$("#proximity-icon-id").empty();
			$("#proximity-icon-id").append(generateProximityIcon(Number.parseFloat($("#proximity-slider-id").val())));
			window.location.search = changeUrlParam("pro",Number.parseFloat($("#proximity-slider-id").val()));
		}
	});

});

function changeUrlParam(paramname, paramvalue) {
	var paramList = URL_PARAMETERS.split('?')[1].split('&');
	var newUrl = "?";
	for (var i = 0 ; i < paramList.length ; i++) {
		if (paramList[i].startsWith(paramname)) {
			newUrl += paramname + "=" + paramvalue;
		} else {
			newUrl += paramList[i];
		}
		if (i<paramList.length-1) {
			newUrl += '&';
		}
	}
	return newUrl;
}

function setSliderValues() {
	var paramsList = window.location.search.split('?')[1].split('&');
	for (var i = 0 ; i < paramsList.length ; i++) {
		if (paramsList[i].startsWith('rat')) {
			var paramvalue = paramsList[i].split('=')[1];
			if (paramvalue != undefined) {
				$("#ratings-slider-id").val(Number.parseFloat(paramvalue));
				$("#ratings-icon-id").empty();
				$("#ratings-icon-id").append(generateStarRatingCode(Number.parseFloat($("#ratings-slider-id").val())));
			} else {
				$("#ratings-slider-id").val(3);
				$("#ratings-icon-id").empty();
				$("#ratings-icon-id").append(generateStarRatingCode(Number.parseFloat($("#ratings-slider-id").val())));
			}
		} else if (paramsList[i].startsWith('bud')) {
			var paramvalue = paramsList[i].split('=')[1];
			if (paramvalue != undefined) {
				$("#budget-slider-id").val(Number.parseInt(paramvalue));
				$("#budget-icon-id").empty();
				$("#budget-icon-id").append(generatePriceRange(Number.parseInt($("#budget-slider-id").val())));

			} else {
				$("#budget-slider-id").val(1);
				$("#budget-icon-id").empty();
				$("#budget-icon-id").append(generatePriceRange(Number.parseInt($("#budget-slider-id").val())));
			}
		} else if (paramsList[i].startsWith('rev')) {
			var paramvalue = paramsList[i].split('=')[1];
			if (paramvalue != undefined) {
				$("#reviews-slider-id").val(Number.parseInt(paramvalue));
				$("#reviews-icon-id").empty();
				$("#reviews-icon-id").append(generateReviewsIcon(Number.parseInt($("#reviews-slider-id").val())));
			} else {
				$("#reviews-slider-id").val(50);
				$("#reviews-icon-id").empty();
				$("#reviews-icon-id").append(generateReviewsIcon(Number.parseInt($("#reviews-slider-id").val())));
			}
		} else if (paramsList[i].startsWith('pro')) {
			var paramvalue = paramsList[i].split('=')[1];
			if (paramvalue != undefined) {
				$("#proximity-slider-id").val(Number.parseFloat(paramvalue));
				$("#proximity-icon-id").empty();
				$("#proximity-icon-id").append(generateProximityIcon(Number.parseFloat($("#proximity-slider-id").val())));
			} else {
				$("#proximity-slider-id").val(3);
				$("#proximity-icon-id").empty();
				$("#proximity-icon-id").append(generateProximityIcon(Number.parseFloat($("#proximity-slider-id").val())));
			}
		}
	}
}

function returnLatLng(urlParams) {
	var map_center;
	var lat;
	var lng;
	for (var i = 0;i<urlParams.length ; i++){
		if(urlParams[i].startsWith('lat')) {
			console.log(urlParams[i].split('='));
			lat = urlParams[i].split('=')[1];
		} else if (urlParams[i].startsWith('lng')) {
			console.log(urlParams[i].split('='));
			lng = urlParams[i].split('=')[1];
		}
	};
	if (lat != false && lng != false) {
		map_center = new google.maps.LatLng(lat,lng);
	} else {
		map_center = new google.maps.LatLng(DEFAULT_LAT,DEFAULT_LNG);
	}
	return map_center;
}

function generateStarRatingCode(stars) {
	var starsCode = "";
	if (stars === 0) {
		starsCode = "<span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	} else if (stars === 0.5) {
		starsCode = "<span class='colored-star'><i class='fa fa-star-half-o'></i></span><span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	} else if (stars === 1) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i></span><span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	} else if (stars === 1.5) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i></span><span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	} else if (stars === 2) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i></span><span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	} else if (stars === 2.5) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i></span><span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	} else if (stars === 3) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i></span><span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	} else if (stars === 3.5) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i></span><span class='grey-star'><i class='fa fa-star-o'></i></span>";
	} else if (stars === 4) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i></span><span class='grey-star'><i class='fa fa-star-o'></i></span>";
	} else if (stars === 4.5) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i></span>";
	} else if (stars === 5) {
		starsCode = "<span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i></span>";
	} else {
		starsCode = "<span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span>";
	};
	return starsCode;
}

// Reviews: 0-10;10-50;50-100;100-250;250-500
function generateReviewsIcon(reviews) {
	var reviewsCode = "";
	if (reviews === 0) {
		reviewsCode = "<span class='grey-thumbs-up'><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i></span>";
	} else if (reviews <= 10) {
		reviewsCode = "<span class='colored-thumbs-up'><i class='fa fa-thumbs-up'></i></span><span class='grey-thumbs-up'><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i></span>";
	} else if (reviews > 10 && reviews <= 50) {
		reviewsCode = "<span class='colored-thumbs-up'><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i></span><span class='grey-thumbs-up'><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i></span>";
	} else if (reviews > 50 && reviews <= 100) {
		reviewsCode = "<span class='colored-thumbs-up'><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i></span><span class='grey-thumbs-up'><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i></span>";
	} else if (reviews > 100 && reviews <= 250) {
		reviewsCode = "<span class='colored-thumbs-up'><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i></span><span class='grey-thumbs-up'><i class='fa fa-thumbs-o-up'></i></span>";
	} else if (reviews > 250) {
		reviewsCode = "<span class='colored-thumbs-up'><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i></span>";
	} else {
		reviewsCode = "<span class='grey-thumbs-up'><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i></span>";
	};
	return reviewsCode;
}

//Proximity: 0-1;1-5;5-10
function generateProximityIcon(proximity) {
	var proximityCode = "";
	if (proximity <= 1) {
		proximityCode = "<span><i class='fa fa-street-view'></i></span>";
	} else if (proximity > 1 && proximity <= 5) {
		proximityCode = "<span><i class='fa fa-bicycle'></i></span>";
	} else if (proximity > 5) {
		proximityCode = "<span><i class='fa fa-car'></i></span>";
	} else {
		proximityCode = "<span><i class='fa fa-car'></i></span>";
	}
	return proximityCode;
}

function generateBusinessOpenCode(open) {
	var openCode = "";
	if (open) {
		openCode = "<div class='col-xs-3 no-padding text-right'><div class='business-open'><i class='fa fa-unlock'></i></div></div>";
	} else {
		openCode = "<div class='col-xs-3 no-padding text-right'><div class='business-closed'><i class='fa fa-lock'></i></div></div>"
	}
	return openCode;
}

function generatePriceRange(priceRange) {
	var priceRangeCode = "";
	if (priceRange === 0) {
		priceRangeCode = "<span class='grey-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span>";
	} else if (priceRange === 1) {
		priceRangeCode = "<span class='colored-usd'><i class='fa fa-usd'></i></span><span class='grey-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span>";
	} else if (priceRange === 2) {
		priceRangeCode = "<span class='colored-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span><span class='grey-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span>";
	} else if (priceRange === 3) {
		priceRangeCode = "<span class='colored-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span><span class='grey-usd'><i class='fa fa-usd'></i></span>";
	} else if (priceRange === 4) {
		priceRangeCode = "<span class='colored-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span>";
	} else {
		priceRangeCode = "<span class='grey-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span>";
	}
	return priceRangeCode;
}

function resultsItemCodeEnd() {
	return "</div></div></div></div></div></div>";
}

function createMarker(lat, lng, name, hoverTarget, markerData, yScore) {
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: GOOG_MAP,
		icon: './img/mapicons/number_'+ yScore + '.png'
	});
	google.maps.event.addListener(marker, 'mouseover', (function(marker) {
		return function() {
			var content = "<div class='map-marker-title'><b>" + name + "</b></div><div id='_" + hoverTarget + "-marker' style='text-align: center;'></div>";
			GOOG_MAPS_INFO_WINDOW.setContent(content);
			GOOG_MAPS_INFO_WINDOW.open(GOOG_MAP, marker);
			createCustomMarker("#_"+hoverTarget + "-marker",markerData);
			//reset all the item description
    		$(".results-item-description").addClass("hidden");
    		//Make the respective description visible
    		$("#"+hoverTarget+"-desc").removeClass("hidden");
		};
	})(marker));
	google.maps.event.addListener(marker, 'mouseout', (function(marker) {
		return function() {
			GOOG_MAPS_INFO_WINDOW.close();
		};
	})(marker));
}

function createCustomMarker (targetId, marker) {
	var data = marker;
	var width = 160,
    height = 160,
    radius = (Math.min(width, height)-20) / 2,
    innerRadius = 0.4 * radius;

	var pie = d3.layout.pie()
	    .sort(null)
	    .value(function(d) { return d.width; });

	var tip = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([0, 0])
	  .html(function(d) {
	    return d.data.label + ": <span style='color:orangered'>" + d.data.score + "</span>";
	  });

	var arc = d3.svg.arc()
	  .innerRadius(innerRadius)
	  .outerRadius(function (d) {
	    return (radius - innerRadius) * (d.data.score / 100.0) + innerRadius;
	  });

	var outlineArc = d3.svg.arc()
	        .innerRadius(innerRadius)
	        .outerRadius(radius);

	var svg = d3.select(targetId).append("svg")
	    .attr("width", width)
	    .attr("height", height)
	    .append("g")
	    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	svg.call(tip);

	data.forEach(function(d) {
	    d.id     =  d.id;
	    d.order  = +d.order;
	    d.color  =  d.color;
	    d.weight = +d.weight;
	    d.score  = +d.score;
	    d.width  = +d.weight;
	    d.label  =  d.label;
	  });

	  var path = svg.selectAll(".solidArc")
	      .data(pie(data))
	    .enter().append("path")
	      .attr("fill", function(d) { return d.data.color; })
	      .attr("class", "solidArc")
	      .attr("stroke", "gray")
	      .attr("d", arc)
	      .on('mouseover', tip.show)
	      .on('mouseout', tip.hide)
	      ;

	      var pathtext = svg.selectAll(".solidArc")
	        .text("Star");

	  var outerPath = svg.selectAll(".outlineArc")
	      .data(pie(data))
	    .enter().append("path")
	      .attr("fill", "none")
	      .attr("stroke", "gray")
	      .attr("class", "outlineArc")
	      .attr("d", outlineArc);


	  // calculate the weighted mean score
	  var score =
	    data.reduce(function(a, b) {
	      //console.log('a:' + a + ', b.score: ' + b.score + ', b.weight: ' + b.weight);
	      return a + (b.score * b.weight);
	    }, 0) /
	    data.reduce(function(a, b) {
	      return a + b.weight;
	    }, 0);

	  svg.append("svg:text")
	    .attr("class", "aster-score")
	    .attr("dy", ".35em")
	    .attr("text-anchor", "middle")
	    .text(Math.round(score));
}
