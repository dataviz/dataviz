<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap-select.min.css">
  <link rel="stylesheet" href="css/jquery.nouislider.min.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/app.css">
  <!-- <link rel="stylesheet" href="css/main.css"> -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDocvVb6Y3yTWwy0a6jRo3R3UMI01-Vd3s&amp;sensor=true"></script>
</head>
<body>
<!-- Header -->
<header id="navbar-top" class="banner navbar navbar-default" role="banner">
  <div class="pannello-affix affix-top dark" data-spy="affix" data-offset-top="79">
    <div class="container">
      <div class="navbar-inner">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="brand" href="/">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        </a>
        <a class="brand-piccolo" href="/">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        </a>
        <nav class="collapse navbar-collapse" role="navigation">
          <ul class="nav navbar-nav">
            <li class="menu-about"><a href="/about/" class=" roll"><span data-title="About">About</span></a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
<!-- content -->
<body class="wrapper">
<!-- Map -->
<div class="hero-wrapper">
  <div id="map"></div>
</div>
<div class="search-places-form-wrapper">
  <div>
    <form id="search-places-form" class="" method="get" action="results.jsp">
      <div class="row">
        <!--Begin: Search places form input attributes -->
        <div class="col-xs-10">
          <input type="hidden" name="rat" value>
          <input type="hidden" name="bud" value>
          <input type="hidden" name="rev" value>
          <input type="hidden" name="pro" value>
          <input id="hidden-lat" type="hidden" name="lat" value="33.4236052">
          <input id="hidden-lng" type="hidden" name="lng" value=" -111.9395536">
          <div class="row">
            <select name="category" class="selectpicker col-xs-11">
              <option value="Restaurants">Restaurants</option>
              <option value="Shopping">Shopping</option>
              <option value="Arts & Entertainment">Art &amp; Entertainment</option>
              <option value="Breakfast & Brunch">Breakfast &amp; Brunch</option>
              <option value="Breweries">Breweries</option>
              <option value="Hospitals">Hospitals</option>
            </select>
            <div class="col-xs-1"></div>
          </div>
        </div>
        <div class="col-xs-2"><button id="search-places-btn" class="btn btn-default" style="float: right; width: 155%;">Search</button></div>
        <!--End: Search places form input atributes -->
        <!--Begin: Preference menu list -->
        <div class="col-xs-12" style="padding-top: 10px;">
          <ul class="preference-row">
            <li style="color: white;">Set your preferences:</li>
            <li><a href="#" id="preference-rating-menu" data-target="preference-rating-slider">Rating</a></li>
            <li><a href="#" id="preference-budget-menu" data-target="preference-budget-slider">Budget</a></li>
            <li><a href="#" id="preference-review-menu" data-target="preference-review-slider">Review</a></li>
            <li><a href="#" id="preference-proximity-menu" data-target="preference-proximity-slider">Proximity</a></li>
          </ul>
        </div>
      </div>
      <!-- End: Preference menu list -->
      <!-- Begin: Code for preference slider box, which appeas on hovering over the preference menu -->
      <div class="preference-rating-slider preference-slider hidden">
        <div class="arrow-mark"></div>
        <div class="row">
          <div class="col-sm-5 center"><span class="slider-icon"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></div>
          <div class="preference-slider-label col-sm-7">Rating</div>
        </div>
        <div class="preference-slider-wrapper"><div style="margin-left: 15px;" class="ratingSlider"></div></div>
      </div>
      <div class="preference-budget-slider preference-slider hidden">
        <div class="arrow-mark"></div>
        <div class="row">
          <div class="col-sm-5 center"><span class="slider-icon"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></div>
          <div class="col-sm-7" style="color: #000; padding: 15px 0; font-size: 18px;">Budget</div>
        </div>
        <div class="preference-slider-wrapper"><div style="margin-left: 15px;" class="ratingSlider"></div></div>
      </div>
      <div class="preference-review-slider preference-slider hidden">
        <div class="arrow-mark"></div>
        <div class="row">
          <div class="col-sm-5 center"><span class="slider-icon"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></div>
          <div class="col-sm-7" style="color: #000; padding: 15px 0; font-size: 18px;">Review</div>
        </div>
        <div class="preference-slider-wrapper"><div style="margin-left: 15px;" class="ratingSlider"></div></div>
      </div>
      <div class="preference-proximity-slider preference-slider hidden">
        <div class="arrow-mark"></div>
        <div class="row">
          <div class="col-sm-5 center"><span class="slider-icon"><span class="glyphicon glyphicon-road" aria-hidden="true"></span></div>
          <div class="col-sm-7" style="color: #000; padding: 15px 0; font-size: 18px;">Proximity</div>
        </div>
        <div class="preference-slider-wrapper"><div style="margin-left: 15px;" class="ratingSlider"></div></div>
      </div>
      <!--End: Preference slider box -->
    </form>
  </div>
</div>

<!-- <a style="float:right;height: 100px;width: 100px; position: absolute; bottom: 10px;right: 10px;" href="javascript:showLocationModal()"  onclick="showLocationModal()">
    <img src="http://simpleicon.com/wp-content/uploads/map-marker-8.png" height="100px" width="100px"/>
</a>
-->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Set Location</h4>
      </div>
      <form id="location-form">
        <div class="modal-body">
          <input type="text" name="address01" class="form-control"  style="margin-bottom: 10px;" placeholder="Address 01"/>
          <input type="text" name="address02"  class="form-control" style="margin-bottom: 10px;" placeholder="Address 02"/>
          <div class="row">
            <select name="map-cities" class="selectpicker col-xs-12">
              <option value="Tempe, AZ, USA">Tempe, AZ, USA</option>
              <option value="Phoenix, AZ, USA">Phoenix, AZ, USA</option>
              <option value="Scottsdale, AZ, USA">Scottsdale, AZ, USA</option>
              <option value="Florence, Italy">Florence, Italy</option>
              <option value="Edinburgh, UK">Edinburgh, UK</option>
              <option value="Waterloo, Canada">Waterloo, Canada</option>
            </select>
          </div>
          <div id="address" style="display: none;"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="location-submit-btn" type="button" class="btn btn-primary">Set Location</button>
        </div>
      </form>


    </div>
  </div>
</div>
</div>
<!-- Footer -->
<!-- <footer>
  <div class="container aligncenter">
    <hr>
    <p class="copyright"> &copy; 2015 ASU</p>
  </div>
</footer> -->
<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/mapmarker.js"></script>
<script src="js/main.js"></script>
</body>
</html>
