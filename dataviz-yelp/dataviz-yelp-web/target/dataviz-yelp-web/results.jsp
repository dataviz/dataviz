<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/jquery.nouislider.min.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/app.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- <link rel="stylesheet" href="css/main.css"> -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDocvVb6Y3yTWwy0a6jRo3R3UMI01-Vd3s&amp;sensor=false"></script>
  <script id="results-list-items-template-begin" type="text/x-handlebars-template">
    <div data-target="{{business_id}}-desc" class="results-item row row-no-margin">
      <div class="col-xs-12 padding-lr5">
        <div class="results-item-wrapper">
          <div class="row row-no-margin">
            <div class="col-xs-4 no-padding">
              <img style="width: 100%; height: 100%;" src="{{image_url}}">
            </div>
            <div class="col-xs-8 padding-lr5">
              <div class="business-name">{{name}}</div>
              <div class="star-lock-wrapper row row-no-margin">
                <div class="col-xs-9 no-padding">
  </script>
  <script id="results-list-items-details-template-begin" type="text/x-handlebars-template">
    <div id="{{business_id}}-desc" class="hidden results-item-description">
      <div class="row row-no-margin">
        <div class="row row-no-margin">
          <div class="col-xs-7">
            <div class="business-name">{{name}}</div>
          </div>
          <div class="col-xs-5">
            <div class="business-yelp-score">
              <span>{{yscore}}</span>
            </div>
          </div>
        </div>
        <div class="row row-no-margin padding-tp5">
          <div class="col-xs-7">
            <div>
  </script>
  <script id="results-list-items-details-template-end" type="text/x-handlebars-template">
    </div>
    <div class="row row-no-margin">
      <div class="reviews-row">
        <div class="col-xs-6">
          <div><span>Review:</span>&nbsp;<span>{{review_count}}</span></div>
        </div>
        <div class="col-xs-3 thumbs-up">
          <i class="fa fa-thumbs-up"><span>{{posReviews}}</span></i>
        </div>
        <div class="col-xs-3 thumbs-down">
          <i class="fa fa-thumbs-down"><span>{{negReviews}}</span></i>
        </div>
      </div>
    </div>
    <ul>TIPS
      {{#each tips}}
      <li>{{this}}</li>
      {{/each}}
    </ul>
    </div>
    </div>
  </script>
</head>
<body>
<!-- Header -->
<header id="navbar-top" class="banner navbar navbar-default" role="banner">
  <div class="pannello-affix dark affix" data-spy="affix">
    <div class="container">
      <div class="navbar-inner">
        <a class="brand" href="/dataviz-yelp-web/index.jsp">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        </a>
        <a class="brand-piccolo" href="/dataviz-yelp-web/index.jsp">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        </a>
        <nav class="collapse navbar-collapse" role="navigation">
          <ul class="nav navbar-nav">
            <li class="menu-about"><a href="https://bitbucket.org/dataviz/dataviz" class=" roll"><span data-title="About">Source Code</span></a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
<!-- <header id="navbar-top" class="banner navbar navbar-default" role="banner">
  <div class="pannello-affix affix-top dark" data-spy="affix">
   <div class="container">
    <div class="navbar-inner">
        <a class="brand" href="/">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        </a>
        <a class="brand-piccolo" href="/">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        </a>
        <nav class="collapse navbar-collapse" role="navigation">
          <ul class="nav navbar-nav">
            <li class="menu-about"><a href="/about/" class=" roll"><span data-title="About">About</span></a></li>
          </ul>
        </nav>
     </div>
   </div>
  </div>
</header> -->
<!-- content -->
<div class="wrapper">

  <!-- Results Column-->
  <div class="results-list-col-wrapper">
    <!-- Results/Places list col-->
    <div class="results-list-col">
      <div class="row row-no-margin">
        <div id="results-list-id"></div>
      </div>
    </div>
  </div>
</div>

<!-- Map Column: Its of fixed width and height-->
<div class="results-map-col-wrapper">
  <div class="hero-wrapper">
    <div id="map" data-target="results"></div>
  </div>
</div>

<!-- Reults preference and selected business details column -->
<div class="preference-bizdetails-wrapper">
  <!-- Result preference row-->
  <div class="preference-col">
    <div class="preference-item">
      <div class="row row-no-margin">
        <div class="col-xs-12" style=""><span>Rating</span></div>
        <div class="col-xs-8"><div class="preference-rating-slider" style="margin-top: 10px;"><div id="ratings-slider-id" class="ratingSlider"></div></div></div>
        <div class="col-xs-4" style=""><span id="ratings-icon-id"><span class='colored-star'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i></span><span class='grey-star'><i class='fa fa-star-o'></i><i class='fa fa-star-o'></i></span></span></div>
      </div>
    </div>
    <hr class="preference-divider divider">
    <div class="preference-item">
      <div class="row row-no-margin">
        <div class="col-xs-12" style=""><span>Budget</span></div>
        <div class="col-xs-8"><div class="preference-budget-slider" style="margin-top: 10px;"><div id="budget-slider-id" class="budgetSlider"></div></div></div>
        <div class="col-xs-4" style=""><span id="budget-icon-id"><span class='colored-usd'><i class='fa fa-usd'></i></span><span class='grey-usd'><i class='fa fa-usd'></i><i class='fa fa-usd'></i><i class='fa fa-usd'></i></span></span></div>
      </div>
    </div>
    <hr class="preference-divider divider">
    <div class="preference-item">
      <div class="row row-no-margin">
        <div class="col-xs-12" style=""><span>Review</span></div>
        <div class="col-xs-8"><div class="preference-review-slider" style="margin-top: 10px;"><div id="reviews-slider-id" class="reviewSlider"></div></div></div>
        <div class="col-xs-4" style=""><span id="reviews-icon-id"><span class='colored-thumbs-up'><i class='fa fa-thumbs-up'></i><i class='fa fa-thumbs-up'></i></span><span class='grey-thumbs-up'><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i><i class='fa fa-thumbs-o-up'></i></span></span></div>
      </div>
    </div>
    <hr class="preference-divider divider">
    <div class="preference-item">
      <div class="row row-no-margin">
        <div class="col-xs-12" style=""><span>Proximity</span></div>
        <div class="col-xs-8"><div class="preference-proximity-slider" style="margin-top: 10px;"><div id="proximity-slider-id" class="proximitySlider"></div></div></div>
        <div class="col-xs-4" style=""><span id="proximity-icon-id"><span><i class="fa fa-bicycle"></i></span></span></div>
      </div>
    </div>
  </div>
  <!-- Business details: Show on hover, else show the top most -->
  <div class="business-details-col">
    <div id="business-details-items-list-id"></div>
  </div>
</div>
</div>
<!-- Footer -->
<!-- <footer>
  <div class="container aligncenter">
    <hr>
    <p class="copyright"> &copy; 2015 ASU</p>
  </div>
</footer> -->
<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/handlebars-v2.0.0.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="http://d3js.org/d3.v3.min.js"></script>
<script src="http://labratrevenge.com/d3-tip/javascripts/d3.tip.v0.6.3.js"></script>
<script src="js/main.js"></script>
<script src="js/results-template.js"></script>
</body>
</html>